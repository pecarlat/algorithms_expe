#!/usr/bin/env python3

import numpy as np

class Graph:

    def __init__(self, nb_vertices, is_undirected=True):
        self.__nb_vertices = nb_vertices
        self.__graph = np.zeros((self.__nb_vertices, self.__nb_vertices))
        self.__vertices_mapping = []
        self.__is_undirected = is_undirected

    # Gets the idx of a vertex given its name
    def get_idx_or_add(self, vertex_name):
        if vertex_name in self.__vertices_mapping:
            return self.__vertices_mapping.index(vertex_name)
        else:
            self.__vertices_mapping.append(vertex_name)
            return len(self.__vertices_mapping) - 1

    # Adds an edge to the graph
    def add_edge(self, vertex_1, vertex_2, weight=1):
        idx_vertex_1 = self.get_idx_or_add(vertex_1)
        idx_vertex_2 = self.get_idx_or_add(vertex_2)

        self.__graph[idx_vertex_1, idx_vertex_2] = weight
        # If undirected, replicate
        if self.__is_undirected:
            self.__graph[idx_vertex_2, idx_vertex_1] = weight

    # Console visualisation
    def print_graph(self):
        for i, vertex in enumerate(self.__graph):
            print(self.__vertices_mapping[i], 'is connected to:')
            print(', '.join([str(self.__vertices_mapping[j]) +
                             ' (weighted = ' + str(vertex[j]) + ')'
                             for j in range(self.__nb_vertices) if vertex[j]]))

    # Console visualisation
    def print_raw_graph(self):
        print(self.__graph)


# Driver program to the above graph class
if __name__ == "__main__":
    number_vertices = 5
    graph = Graph(number_vertices)
    graph.add_edge('a', 1)
    graph.add_edge('a', 4)
    graph.add_edge(1, 'b')
    graph.add_edge(1, 3)
    graph.add_edge(1, 4)
    graph.add_edge('b', 3)
    graph.add_edge(3, 4)

    graph.print_graph()
    graph.print_raw_graph()