#!/usr/bin/env python3

from collections import defaultdict

class Graph:

    def __init__(self, nb_vertices, is_undirected=True):
        self.__nb_vertices = nb_vertices
        self.__graph = defaultdict(dict)
        self.__is_undirected = is_undirected

    # Adds an edge to the graph
    def add_edge(self, vertex_1, vertex_2, weight=1):
        self.__graph[vertex_1][vertex_2] = weight
        # If undirected, replicate
        if self.__is_undirected:
            self.__graph[vertex_2][vertex_1] = weight

    # Console visualisation
    def print_graph(self):
        for vertex_name, connections in self.__graph.items():
            print(vertex_name, 'is connected to:')
            print(', '.join([str(v) + ' (weighted = ' + str(w) + ')'
                             for v, w in connections.items()]))

    # Console visualisation
    def print_raw_graph(self):
        print(self.__graph)

    # Returns the names of the vertices
    def get_vertices(self):
        return self.__graph.keys()

    # Returns the connections from a vertex
    def at(self, vertex):
        return self.__graph[vertex]


# Driver program to the above graph class
if __name__ == "__main__":
    number_vertices = 5
    graph = Graph(number_vertices)
    graph.add_edge('a', 1)
    graph.add_edge('a', 4)
    graph.add_edge(1, 'b')
    graph.add_edge(1, 3)
    graph.add_edge(1, 4)
    graph.add_edge('b', 3)
    graph.add_edge(3, 4)

    graph.print_graph()
    graph.print_raw_graph()