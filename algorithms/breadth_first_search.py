#!/usr/bin/env python3

from graphs.graph_dict import Graph

# Does a BFS to find the shortest path from vertex_1 to vertex_2
def breadth_first_search(graph, vertex_1, vertex_2):
    distances = {v: -1 for v in graph.get_vertices()}
    distances[vertex_1] = 0
    queue = list(graph.at(vertex_1).keys())
    queue_distances = [1] * len(queue)
    while queue:
        distances[queue[0]] = queue_distances[0]
        if queue[0] == vertex_2:
            break
        for new_vertex in graph.at(queue[0]).keys():
            if new_vertex not in queue:
                queue.append(new_vertex)
                queue_distances.append(queue_distances[0] + 1)
        queue = queue[1:]
        queue_distances = queue_distances[1:]
    print(distances[vertex_2])

# Driver program to the above graph class
if __name__ == "__main__":
    number_vertices = 5
    graph = Graph(number_vertices)
    graph.add_edge('a', 1)
    graph.add_edge('a', 4)
    graph.add_edge(1, 'b')
    graph.add_edge(1, 3)
    graph.add_edge(1, 4)
    graph.add_edge('b', 3)
    graph.add_edge(3, 4)

    shortest = breadth_first_search(graph, 'a', 3)
    print(shortest)