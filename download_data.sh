DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

mkdir -p $DIR/data
cd $DIR/data
wget https://algs4.cs.princeton.edu/44sp/tinyEWD.txt
wget https://algs4.cs.princeton.edu/44sp/mediumEWD.txt
wget https://algs4.cs.princeton.edu/44sp/1000EWD.txt
wget https://algs4.cs.princeton.edu/44sp/10000EWD.txt
wget https://algs4.cs.princeton.edu/44sp/largeEWD.txt
cd -