# Experiments on simple algorithms
Some experiments on how to implement simple everyday algorithms. Also here to
help me compare their efficiency and to understand how to use them in real
projects.

# Data
The data used is proposed by the 4th edition of `Algorithms`, from Kevin Wayne
and Bob Sedgewick. Don't forget to download it before using any script.
```
bash download_data.sh
```

# Algorithms
## Breadth-First search
Expands the frontier between discovered and undiscovered vertices uniformly
across the breadth of the frontier. That is, the algorithm discovers all
vertices at distance `k` before discovering any vertices at distance `k + 1`.
